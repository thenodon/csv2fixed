.CURDIR=`pwd`
BIN=bin
DIST=dist
SRC=src
BIN=bin
EXAMPLE=example
PACK=csv2fixed
VERS=0.3.4

all: bin 
	@echo "--------------------------------------------------------------"
	@echo " Make all"
	@echo "--------------------------------------------------------------"
	@cd ${SRC} && ${MAKE} ${.MAKEFLAGS} all
	
bin: 
	@mkdir ${BIN}

clean:
	@echo "--------------------------------------------------------------"
	@echo " Clean all"
	@echo "--------------------------------------------------------------"
	@cd ${SRC} && ${MAKE} ${.MAKEFLAGS} clean
	@rm -rf ${DIST}/

winclean:
	@echo "--------------------------------------------------------------"
	@echo " Clean win all"
	@echo "--------------------------------------------------------------"
	@cd ${SRC} && ${MAKE} ${.MAKEFLAGS} winclean
	@rmdir $(DIST) /Q /S 
	
dist: clean 
	@echo "--------------------------------------------------------------"
	@echo " Build dist"
	@echo "--------------------------------------------------------------"
	@cd ${SRC} && ${MAKE} ${.MAKEFLAGS} all
	@mkdir -p ${DIST}/${PACK}.${VERS}
	@cp -a  ${BIN}/ ${DIST}/${PACK}.${VERS}/
	@cp -a  ${SRC}/ ${DIST}/${PACK}.${VERS}/
	@cp -a  ${EXAMPLE}/ ${DIST}/${PACK}.${VERS}/
	@cp README INSTALLATION Makefile ${DIST}/${PACK}.${VERS}/
	@cd ${DIST} && zip -r ${PACK}_linux.${VERS}.zip  . -x "*/.svn/*" 

windist: winclean
	@echo "--------------------------------------------------------------"
	@echo " Build win dist"
	@echo "--------------------------------------------------------------"
	@cd ${SRC} && ${MAKE} ${.MAKEFLAGS} all
	@mkdir ${DIST}\${PACK}.${VERS}
	@mkdir ${DIST}\${PACK}.${VERS}\bin
	@mkdir ${DIST}\${PACK}.${VERS}\src
	@mkdir ${DIST}\${PACK}.${VERS}\example
	@copy ${BIN} ${DIST}\${PACK}.${VERS}\bin 
	@copy ${SRC} ${DIST}\${PACK}.${VERS}\src 
	@copy  ${EXAMPLE} ${DIST}\${PACK}.${VERS}\example 
	@copy README  ${DIST}\${PACK}.${VERS}
	@copy INSTALLATION ${DIST}\${PACK}.${VERS}
	@copy Makefile ${DIST}\${PACK}.${VERS}
	@cd ${DIST} && c:\Program\7-Zip\7za a ${PACK}_win.${VERS}.zip .  -x!*.svn*
	
