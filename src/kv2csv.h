/*
 * kv2csv.h
 *
 *  Created on: Dec 14, 2009
 *      Author: andersh
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>

#include "hashtable.h"
#include "hashtable_itr.h"


#define MAXRECORDS 1024


/*****************************************************************************/
struct key {
    char *keyname;

};

struct value {
    char *thevalue[MAXRECORDS];

};


