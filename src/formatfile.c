/*
 #
 # Copyright (C) 2009 Ingenjorsbyn AB
 #
 # This program is free software: you can redistribute it and/or modify
 # it under the terms of the GNU General Public License as published by
 # the Free Software Foundation, either version 2 of the License, or
 # (at your option) any later version.
 #
 # This program is distributed in the hope that it will be useful,
 # but WITHOUT ANY WARRANTY; without even the implied warranty of
 # MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 # GNU General Public License for more details.
 #
 # You should have received a copy of the GNU General Public License
 # along with this program.  If not, see <http://www.gnu.org/licenses/>.
 #

 */

/*
 * formatfile.c
 *
 *  Created on: Sep 12, 2009
 *      Author: andersh
 */


#include "csv2fixed.h"
#include "zeromalloc.h"
#include "formatfile.h"

char *windex(char *s, int c);


void printrecord(rec)
record *rec;
{
    printf("NumOfFields %d\n", rec->numoffields);
    printf("Total record length is %d bytes\n", calcrecordlen(rec));
}

int calcrecordlen(rec)
record *rec;
{
    int i = 0;
    int recordlen = 0;
    
    for (i = 0; i < rec->numoffields; i++) {
        if (rec->ind[i] == 1)
            recordlen = recordlen + calcformatlen(rec->format[i]);
    }
    return recordlen;
}


int calcformatlen(str)
char *str;
{
    // T%-10.10s where .10 indicate the length
    char *ptr1;
    char *ptr2;

    ptr1 = (char *)zeromalloc(strlen(str) + 1);
    strcpy(ptr1, str);

    while (*ptr1 != '\0' && *ptr1 != '.')
        ptr1++;

    if (*ptr1 == '.') {
        ptr1++;
        ptr2 = ptr1;
        while (isdigit(*ptr2) != 0 )
            ptr2++;
        *ptr2 = '\0';
        return atoi(ptr1);
    }else
        return 0;
}

/*
    The function read a format string and parse it for processing
 */
record *formatparse(fptr)
char *fptr;
{
    int i = 0;
    int numoffields = 0;
    char *tmp = NULL;
    char *tmpst = NULL;
    int pos = 0;
    record *rec = (record *)malloc((int)sizeof(record));

    for (i = 0; i < TOTPOS; i++)
        rec->ind[i] = 0;


    while (*fptr != '\0') {
        // Managing comment lines
        if (*fptr == '#') {
            while (*fptr != '\n')
                fptr++;
            fptr++;
            continue;
        }
        // Manage empty lines
        if (*fptr == '\n') {
            fptr++;
            continue;
        }

        // Managed head structure
        if (strncmp(fptr, "head:", 5) == 0) {
            tmp = fptr;
            fptr = windex(tmp, '\n');
            *fptr = '\0';
            fptr++;

            //Position passed head:
            tmp += 5;
            tmpst = tmp;

            // Check head indicator
            tmp = windex(tmpst, ':');
            *tmp = '\0';
            tmp++;
            rec->head_ind = atoi(tmpst);

            if (rec->head_ind == 1) {
                tmpst = tmp;
                tmp = windex(tmpst, ':');
                *tmp = '\0';
                tmp++;
                rec->head_format = (char *)zeromalloc((int)strlen(tmpst) + 1);
                strcpy(rec->head_format, tmpst);

                tmpst = tmp;
                rec->head_data = (char *)zeromalloc((int)strlen(tmpst) + 1);
                strcpy(rec->head_data, tmpst);
            }
            continue;
        }
        numoffields++;
        tmp = fptr;
        fptr = windex(fptr, '\n');
        *fptr = '\0';
        fptr++;

        // field position
        tmpst = tmp;
        tmp = windex(tmpst, ':');

        *tmp = '\0';
        tmp++;
        pos = atoi(tmpst) - 1;

        // field indicator
        tmpst = tmp;

        tmp = windex(tmpst, ':');
        *tmp = '\0';
        tmp++;
        rec->ind[pos] = atoi(tmpst);

        // Type and format
        if (rec->ind[pos] == 1) {
            tmpst = tmp;
            tmp = windex(tmpst, ':');
            *tmp = '\0';
            tmp++;
            rec->type[pos] = tmpst;

            tmpst = tmp;
            rec->format[pos] = (char *)zeromalloc((int)strlen(tmpst) + 1);
            strcpy(rec->format[pos], tmpst); //, (int) strlen(tmpst));
        }
    }
    rec->numoffields = numoffields;

    return rec;
}
