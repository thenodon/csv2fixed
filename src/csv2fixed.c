/*
 #
 # Copyright (C) 2009 Ingenjorsbyn AB
 #
 # This program is free software: you can redistribute it and/or modify
 # it under the terms of the GNU General Public License as published by
 # the Free Software Foundation, either version 2 of the License, or
 # (at your option) any later version.
 #
 # This program is distributed in the hope that it will be useful,
 # but WITHOUT ANY WARRANTY; without even the implied warranty of
 # MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 # GNU General Public License for more details.
 #
 # You should have received a copy of the GNU General Public License
 # along with this program.  If not, see <http://www.gnu.org/licenses/>.
 #
 */

#include "csv2fixed.h"

// Prototype functions
#include "utils.h"
#include "formatfile.h"
#include "stropr.h"
#include "zeromalloc.h"

void usage(int verbose);
void usageverbose();
int wgetopt(int argc, char **argv, char *opts);
int parseandwrite(record *rec, char *infileptr, FILE *fout, char sep, int verbose, int delfirstline);

char *version = "0.3.5";
void usage(verbose)
int verbose;
{

    printf("csv2fixed -i filename [-o filename] -f filename [-s sep] [-d] [-v] [-u]\n");
    printf("-i file name of the csv file to transform.\n");
    printf("-o file name of the fixed record file. If not set the file is written to stdout.\n");
    printf("-f format file, see below.\n");
    printf("-d delete the first line in the file, typical when used for name of the columns\n");
    printf("-s the column separator character, default ;\n");
    printf("-v verbose processing and adding a : character and newline on the out put file.\n");
    printf("-u show usage. Use with -v to see extended usage\n\n");
    printf("csv2fixed version %s\n\n", version);
    printf("csv2fixed transforms a csv file to a fixed record file.\n");
    printf("Return values\n"
           "1 - File names are not set\n"
           "2 - File do not exist\n"
           "3 - The seperator character is not a single character\n"
           "4 - The field type does not exist\n"
           "5 - A num field include none digits\n"
           "6 - Can not allocate enough memory");

    if (verbose == 1)
        usageverbose();
}

void usageverbose()
{
    printf("The program makes the transformations through the use of a formatting file. The file \n");
    printf("describe how each column in the input csv file should be formatted in the output fixedi\n");
    printf("record file. Example:\n");
    printf("# This is the formatting file for file type 33 \n");
    printf("head:1:%%-7.7s,FILE-33\n");
    printf("1:1:char:%%-9.9s\n");
    printf("2:1:char:%%-35.35s\n");
    printf("3:0::\n");
    printf("\n");
    printf("4:0::\n");
    printf("5:1:char:T%%-4.4s\n");
    printf("\n");
    printf(
        "The formatting file can include comment lines started with #, empty line,\n"
        "header definition and column description. A line that starts with a\n"
        "number is the definition of that column number in the csv file starting\n"
        "from 1.  The second field is an drop indicator. If 0 that column in the \n"
        "csv file will not be processed. The third field is the data type. Type\n"
        "char is a plain string that can include any characters and no\n"
        "transformations are done. The num type is also a string but it checked that\n"
        "the string only include digits 0-9 . The last field is the formatting of\n"
        "that field. This follows normal printf style where %%-35.35s define a fixed\n"
        "size of 35 bytes (.35), aligned left (-) and space padded to right maximum\n"
        "with 35 spaces (35). If the string should be right aligned remove the\n"
        "- sign. There is also some space trimming options that can used before \n"
        "the %% sign. T means remove all spaces. This is good for numeric values.\n"
        "Option t trims only initial spaces before any character in the string. \n"
        "The head entry works like a normal entry with the exception is that the\n"
        "last field is written according to the formatting first in the file.\n"
        );
    printf("\n");
    printf(
        "The program can automatically manage csv files that are dos (CR-NL) and\n"
        "unix (NL)  end of line formatted. \n"
        "\n"
        "Csv2fixed do not manage the following tasks:\n"
        "- Character transformations between different encoding. \n"
        "\n");
}


int main(argc, argv)
int argc;
char **argv;
{


    FILE *fid = NULL;
    FILE *fout = NULL;
    FILE *formatid = NULL;

    int option;
    extern char *optarg;


    struct stat sb;
    char *infileptr = NULL;
    char *formatptr = NULL;
    int delfirstline = 0;

    char sep = ';';
    record *rec = NULL;
    int verbose = 0;
    unsigned int ret = 0;
    int showusage = 0;
    int retstat;

    while ((option = wgetopt(argc, argv, "i:o:s:f:dvu" )) != -1)
        switch (option) {

        // -i the name of in file
        case 'i':
            fid = fopen(optarg, "rb");
            if (fid == NULL) {
                printf("In file do not exist - exit\n");
                exit(2);
            }
            stat(optarg, &sb);
            infileptr = (char *)zeromalloc(sb.st_size + 1);
            if (infileptr == NULL) {
                printf("Can not allocat enough memory - exiting\n");
                exit(6);
            }
            retstat = fread(infileptr, 1, sb.st_size, fid);
            *(infileptr + sb.st_size) = '\0';
            fclose(fid);
            break;
        // -o the name of the out fixed record file
        case 'o':
            fout = fopen(optarg, "w+");
            break;
        // -s the seperator character, default ;
        case 's':
            if (strlen(optarg) > 1) {
                printf("Seprator is bigger then a single character - exiting\n");
                exit(4);
            }
            sep = *optarg;
            break;
        // verbose - traceing and file format with new line end with ":\n"
        case 'v':
            verbose = 1;
            break;
        // format file
        case 'f':
            formatid = fopen(optarg, "r");
            if (formatid == NULL) {
                printf("Format file do not exist - exit\n");
                exit(2);
            }
            stat(optarg, &sb);
            formatptr = (char *)zeromalloc(sb.st_size + 1);
            retstat = fread(formatptr, 1, sb.st_size, formatid);
            *(formatptr + sb.st_size) = '\0';
            formatptr = removeCR(formatptr);
            fclose(formatid);
            break;
        case 'd':
            delfirstline = 1;
            break;
        case 'u':
            showusage = 1;
            break;
        default:
            break;
        }

    if (showusage == 1) {
        usage(verbose);
        exit(0);
    }

    // Check all conditions
    if (fid == NULL ) {
        printf("In file name is not set.\n");
        exit(1);
    }else if (fout == NULL )
        fout = fdopen(1, "w");
    else if (formatid == NULL ) {
        printf("Format file name is not set.\n");
        exit(1);
    }

    rec = formatparse(formatptr);
    if (verbose == 1)
        printrecord(rec);


    ret = parseandwrite(rec, infileptr, fout, sep, delfirstline, verbose);
    fclose(fout);
    
    if (verbose == 1)
        printf("\nTotal rows processed: %d\n", ret);
    
    exit(0);
}

/*
   Parse through the in file memory copy, format and write out
 */

int parseandwrite(rec, infileptr, fout, sep, delfirstline, verbose)
record * rec;
char *infileptr;
FILE *fout;
char sep;
int verbose;
int delfirstline;
{

    int verbosecount = 0;

    int rows = 0;
    int pos = 0;
    int endofline = 0;
    char *parse;
    char *tmp;
    char *trimformat;
    char *trimdata, *trimptr;

    parse = infileptr;
    tmp = infileptr;

    // Remove the first line by position on second line after \n
    // This is independent of dos CR-NL or unix NL format
    if (delfirstline == 1) {
        while (*parse != '\n')
            parse++;
        *parse = '\0';
        parse++;
    }

    // If head, write it
    if (rec->head_ind == 1) {
        fprintf(fout, rec->head_format, rec->head_data);
        if (verbose)
            fprintf(fout, ":\n");
    }
    if (verbose)
        printf("Start processing\n");

    tmp = parse;

    // Parse through until end of memory segment where the in file is located
    while (*parse != '\0') {
        endofline = 0;

        // Check for any special separator character and make correct alignment
        if (*parse == '\n' || *parse == '\r' || *parse == sep ) {

            // Manage CR-NL dos end of line
            if (*parse == '\r' && *(parse + 1) == '\n') {
                endofline = 1;
                *parse = '\0';
                parse++;
            }

            // Manage NL unix end of line
            if (*parse == '\n')
                endofline = 1;

            *parse = '\0';

            // Format and write the found field, if field indicator is 1
            if (rec->ind[pos] == 1 ) {
                // If num data type
                if (strcmp(rec->type[pos], "num") == 0) {

                    trimdata = (char *)zeromalloc(strlen(tmp) + 1);
                    trimptr = trimdata;

                    // trim all spaces in a string
                    while (*tmp != '\0') {
                        if (isdigit(*tmp) != 0 ) {
                            *trimptr = *tmp;
                            trimptr++;
                        } else if (isblank(*tmp) != 0) {
                        }else {
                            printf("Field %d on row %d is num but include none digits - exiting\n", pos, rows + 1);
                            exit(5);
                        }
                        tmp++;
                    }
                    fprintf(fout, rec->format[pos], trimdata);
                }
                // If char data type
                else if (strcmp(rec->type[pos], "char") == 0) {
                    //Check if Trim is set in begining of format string
                    if (*(rec->format[pos]) == 'T') {
                        trimformat = rec->format[pos] + 1;

                        trimdata = (char *)zeromalloc(strlen(tmp) + 1);
                        trimptr = trimdata;

                        // The T trim all spaces in a string
                        while (*tmp != '\0') {
                            if (*tmp != ' ') {
                                *trimptr = *tmp;
                                trimptr++;
                            }
                            tmp++;
                        }
                        // Write out
                        fprintf(fout, trimformat, trimdata);
                    } else if (*(rec->format[pos]) == 't') {
                        trimformat = rec->format[pos] + 1;

                        trimdata = (char *)zeromalloc(strlen(tmp) + 1);
                        trimptr = trimdata;

                        // The t trim all pre space in a string
                        while (*tmp != '\0') {
                            if ( isblank(*tmp) != 0 )
                                tmp++;
                            else
                                break;
                        }
                        // Write out
                        fprintf(fout, trimformat, tmp);
                    } // End trimming
                    else
                        fprintf(fout, rec->format[pos], tmp);
                }else {
                    printf("Can not map field type %s - exit\n", rec->type[pos]);
                    exit(3);
                }
            }

            // Manage a empty field, like ;;
            if (*(parse + 1) != sep)
                parse++;

            // verbose formatting the out file with an NL
            if (endofline == 1) {
                if (verbose)
                    fprintf(fout, ":\n");
                pos = 0;
            }else
                pos++;

            tmp = parse;

        } //if

        parse++;
        if (verbose == 1 && endofline == 1) {

            rows++;
            if (rows % 1000 == 0) {
                printf("=");
                verbosecount++;
                if (verbosecount % 70 == 0)
                    printf("%10.d\n", rows);


                //printf("rows %d\n",rows);
            }
        }
    } //while

    return rows;
}




