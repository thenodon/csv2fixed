/*
 #
 # Copyright (C) 2009 Ingenjorsbyn AB
 #
 # This program is free software: you can redistribute it and/or modify
 # it under the terms of the GNU General Public License as published by
 # the Free Software Foundation, either version 2 of the License, or
 # (at your option) any later version.
 #
 # This program is distributed in the hope that it will be useful,
 # but WITHOUT ANY WARRANTY; without even the implied warranty of
 # MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 # GNU General Public License for more details.
 #
 # You should have received a copy of the GNU General Public License
 # along with this program.  If not, see <http://www.gnu.org/licenses/>.
 #
 */


#include "csv2fixed.h"

// Prototype functions
#include "utils.h"
#include "formatfile.h"
#include "stropr.h"
#include "zeromalloc.h"

// Prototype functions
void usage(int verbose);
void usageverbose();
int wgetopt(int argc, char **argv, char *opts);
int parseandwrite(record *rec, char *infileptr, FILE *fout, char sep, int delfirstline);

char *version = "0.3.5";

void usage(verbose)
int verbose;
{
    printf("fixed2fixed -i filename [-o filename] -f filename [-r] [-s sep] [-d] [-v] [-u]\n");
    printf("-i file name of the csv file to transform.\n");
    printf("-o file name of the fixed record file. If not set the file is written to stdout.\n");
    printf("-f format file, see below.\n");
    printf("-s the column separator character, default ;\n");
    printf("-d delete the first line in the file. Used for files with headers\n");    
    printf("-r remove any cr and nl in the file, default is not\n");
    printf("-v verbose processing and adding a : character and newline on the out put file.\n");
    printf("-u show usage. Use with -v to see extended usage\n\n");
    printf("csv2fixed version %s\n\n", version);
    printf("csv2fixed transforms a csv file to a fixed record file.\n");
    printf("Return values\n"
           "1 - File names are not set\n"
           "2 - File do not exist\n"
           "3 - The separator character is not a single character\n"
           "4 - The field type does not exist\n"
           "5 - A num field include none digits\n"
           "6 - Can not allocate enough memory\n");

    if (verbose == 1)
        usageverbose();
}

void usageverbose()
{
    printf("Verbose\n");
}

// Global varaibles
int exitOnfault = 0;
int verbose = 0;

int main(argc, argv)
int argc;
char **argv;
{


    FILE *fid = NULL;
    FILE *fout = NULL;
    FILE *formatid = NULL;

    int option;
    extern char *optarg;


    struct stat sb;
    char *infileptr = NULL;
    char *formatptr = NULL;
    int delfirstline = 0;
    int removeallcrnl = 0;

    record *rec = NULL;

    char sep = ';';
    unsigned int ret = 0;
    int showusage = 0;
    int retstat;

    while ((option = wgetopt(argc, argv, "i:o:s:f:dvurF" )) != -1)
        switch (option) {

        // -i the name of in file
        case 'i':
            fid = fopen(optarg, "rb");
            if (fid == NULL) {
                printf("In file do not exist - exit\n");
                exit(2);
            }
            stat(optarg, &sb);
            infileptr = (char *)zeromalloc(sb.st_size + 1);
            if (infileptr == NULL) {
                printf("Can not allocate enough memory - exiting\n");
                exit(6);
            }
            retstat = fread(infileptr, 1, sb.st_size, fid);
            *(infileptr + sb.st_size) = '\0';
            fclose(fid);
            break;
        // -o the name of the out fixed record file
        case 'o':
            fout = fopen(optarg, "w+");
            break;
        // -s the seperator character, default ;
        case 's':
            if (strlen(optarg) > 1) {
                printf("Seprator is bigger then a single character - exiting\n");
                exit(4);
            }
            sep = *optarg;
            break;
        // verbose - traceing and file format with new line end with ":\n"
        case 'v':
            verbose = 1;
            break;
        // format file
        case 'f':
            formatid = fopen(optarg, "r");
            if (formatid == NULL) {
                printf("Format file do not exist - exit\n");
                exit(2);
            }
            stat(optarg, &sb);
            formatptr = (char *)zeromalloc(sb.st_size + 1);
            if (formatptr == NULL) {
                printf("Can not allocate enough memory for format file - exiting\n");
                exit(6);
            }
            retstat = fread(formatptr, 1, sb.st_size, formatid);
            *(formatptr + sb.st_size) = '\0';
            formatptr = removeCR(formatptr);

            fclose(formatid);
            break;
        case 'd':
            delfirstline = 1;
            break;
        case 'r':
            removeallcrnl = 1;
            break;
        case 'u':
            showusage = 1;
            break;
        case 'F':
            exitOnfault = 1;
            break;
        default:
            break;
        }

    if (showusage == 1) {
        usage(verbose);
        exit(0);
    }

    // Check all conditions
    if (fid == NULL ) {
        printf("In file name is not set.\n");
        exit(1);
    }else if (fout == NULL )
        fout = fdopen(1, "w");
    else if (formatid == NULL ) {
        printf("Format file name is not set.\n");
        exit(1);
    }

    // Remove any header lines
    char *parse = infileptr;

    // Remove the first line by position on second line after \n - ONLY possible
    // if the fixed file has newline delimiters.

    // This is independent of dos CR-NL or unix NL format
    if (delfirstline == 1 && removeallcrnl == 1) {
        while (*parse != '\n') {
            parse++;
         }
        *parse = '\0';
        parse++;
        infileptr = parse;
    }


    
    if (removeallcrnl == 1 )
        infileptr = removeCRNL(infileptr);
    
    rec = formatparse(formatptr);
    if (verbose == 1)
        printrecord(rec);
    ret = parseandwrite(rec, infileptr, fout, sep, delfirstline);
    fclose(fout);
    if (verbose == 1)
        printf("\nTotal records processed: %d\n", ret/calcrecordlen(rec));
    exit(0);
}

/*
    Parse through the input file memory copy, format it and write out
    - record - is the entity of single "row" in the input file made up by a 
    number of fields 
    - record size - is the total size of all individual fields size     
 */
int parseandwrite(rec, infileptr, fout, sep, delfirstline)
record * rec;
char *infileptr;
FILE *fout;
char sep;
int delfirstline;
{
    char *parseptr;
    char *recbuf; // the bluffer holding a single transformed record to a csv 
                  // row including csv field delimiters  
                  
    char *recptr; // the pointer to recbuf use creating the recbuf
    
    char *fieldbuf; // the field buffer
    char *fieldptrS; // 
    char *fieldptrE;

    int recordSize = 0; 
    long numberofrecords = 0; 
    int count = 0;
    int i = 0;
    int j = 0;
    
    // Calculate the fixed record size based on each individual field size
    for (i = 0; i < rec->numoffields; i++) {
        if (rec->ind[i] == 1) {
            rec->fieldsize[i] = calcformatlen(rec->format[i]);
            recordSize = recordSize + rec->fieldsize[i];
        }
    }

    

    // Check correctness of the file
    
    ////////////////////////////////
    // Evaluation of the correctness of the input file size compared to the 
    // calculated field size
    if (even(strlen(infileptr),(long) recordSize) != 0) {
        printf("The input file have a size that are not even to the total field size");
        if (exitOnfault == 1) {
            printf (" - exiting\n");
            exit(1);
        }
        else {
            printf("\n");
        }    
    }
    
    ///////////////////////////////    
    // Calculate the number of records 
    numberofrecords = strlen(infileptr) / (long) recordSize;
    
    // Allocate a buf for each line, sise of all data, separator and row end
    recbuf = (char *)zeromalloc(recordSize + rec->numoffields + 2);
    if (recbuf == NULL) {
        printf("Can not allocate enough memory for file buf- exiting\n");
        exit(6);
    }
    
    // Check that you can get it big enough
    fieldbuf = (char *)zeromalloc(recordSize + 1);
    if (fieldbuf == NULL) {
        printf("Can not allocate enough memory for copy buf- exiting\n");
        exit(6);
    }
    

    // Calculate the number of characters in the input memory
    parseptr = infileptr;
    while (*parseptr != '\0') {
        parseptr++;
        count++;
    }

    parseptr = infileptr;

    // Parse through until end of memory segment where the in file is located
    for (j = 0; j < numberofrecords; j++) {
        recptr = recbuf;

        // Loop through each record field
        for (i = 0; i < rec->numoffields; i++) {
            // If the field should be used
            if (rec->ind[i] == 1) {
                fieldptrS = fieldbuf;

                strncpy(fieldptrS, parseptr, rec->fieldsize[i]);
                fieldptrE = fieldptrS + rec->fieldsize[i];
                *fieldptrE = '\0';

               //Trim away all blank inital characters
                while (*fieldptrS != '\0') {
                    if ( isblank(*fieldptrS) != 0 )
                        fieldptrS++;
                    else
                        break;
                }

                // Trim away all  blank trailing characters 
                fieldptrE--;
                while (fieldptrE > fieldptrS) {
                    if ( isblank(*fieldptrE) != 0 )
                        fieldptrE--;
                    else {
                        fieldptrE++;
                        *fieldptrE = '\0';
                        break;
                    }
                }

                // Copy the field to the record
                strcpy(recptr, fieldptrS);
                // Position in the end of the record content
                recptr = recptr + strlen(fieldptrS);
            } 

            
            parseptr = parseptr + rec->fieldsize[i];
            *recptr = sep;
            recptr++;

        } // for loop record 

        recptr--;
        *recptr = '\0';

        fprintf(fout, "%s\n", recbuf);
        zero(recbuf, recordSize + rec->numoffields + 2);
    } //for loop fixed file memory
    return count;
}




