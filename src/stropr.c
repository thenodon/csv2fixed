/*
 #
 # Copyright (C) 2009 Ingenjorsbyn AB
 #
 # This program is free software: you can redistribute it and/or modify
 # it under the terms of the GNU General Public License as published by
 # the Free Software Foundation, either version 2 of the License, or
 # (at your option) any later version.
 #
 # This program is distributed in the hope that it will be useful,
 # but WITHOUT ANY WARRANTY; without even the implied warranty of
 # MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 # GNU General Public License for more details.
 #
 # You should have received a copy of the GNU General Public License
 # along with this program.  If not, see <http://www.gnu.org/licenses/>.
 #

 */

/*include <unistd.h>
 * stropr.c
 *
 *  Created on: Sep 12, 2009
 *      Author: andersh
 */

/*
    A portable index function that even works on windows
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>

#include "zeromalloc.h"

char *windex(s, c)
char *s;
int c;
{
    char *ptr;
    ptr = s;
    while (*ptr != '\0') {
        if (*ptr == c)
            return ptr;
        ptr++;
    }
    return NULL;
}

/*
    Remove all CR that exists in a string.
 */
char *removeCR(s)
char *s;
{
    char *tmp, *ptr, *rem;
    tmp = NULL;
    ptr = NULL;
    rem = s;

    tmp = (char *)zeromalloc(strlen(s) + 1);
    if (tmp == NULL) {
        printf("ran out of memory allocating a remove CR buffer\n");
        _exit(1);
    }
    ptr = tmp;
    while (*rem != '\0') {
        if (*rem != '\r' ) {
            *ptr = *rem;
            ptr++;
        }
        rem++;
    }
    free(s);
    return tmp;
}



/*
    Remove all CR that exists in a string.
 */
char *removeCRNL(s)
char *s;
{
    char *tmp, *ptr, *rem;
    tmp = NULL;
    ptr = NULL;
    rem = s;

    tmp = (char *)zeromalloc(strlen(s) + 1);
    //tmp = (char *) calloc (1,(size_t) strlen(s)+1);
    if (tmp == NULL) {
        printf("ran out of memory allocating a remove CR buffer\n");
        _exit(1);
    }
    ptr = tmp;
    while (*rem != '\0') {
        if (!(*rem == '\r' || *rem == '\n' )) {
            *ptr = *rem;
            ptr++;
        }
        rem++;
    }
    //free(s);
    return tmp;
}

/*
    Remove all CR that exists in a string.
 */
char *formatkv(s, sep)
char *s;
char sep;
{
    char *tmp, *ptr, *rem;
    tmp = NULL;
    ptr = NULL;
    rem = s;

    int onkey = 1;
    int beginkey = 1;

    tmp = (char *)zeromalloc(strlen(s) + 1);
    if (tmp == NULL) {
        printf("ran out of memory allocating a kv buffer\n");
        _exit(1);
    }
    ptr = tmp;
    while (*rem != '\0') {
        if (*rem == '\n') {
            onkey = 1;
            beginkey = 1;
        }

        if (*rem != '\n' && onkey == 1 && beginkey == 1) {
            if (*rem == ' ') {
                rem++;
                continue;
            }else
                beginkey = 0;
        }else if (*rem != '\n' && onkey == 1 && beginkey == 0) {
            // search until sep
            if (*rem != sep && *rem == ' ') {
                rem++;
                continue;
            }else if (*rem == sep)
                onkey = 0;
        }

        *ptr = *rem;
        ptr++;
        rem++;

    }
    free(s);
    return tmp;
}
