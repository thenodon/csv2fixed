/*
 #
 # Copyright (C) 2009 Ingenjorsbyn AB
 #
 # This program is free software: you can redistribute it and/or modify
 # it under the terms of the GNU General Public License as published by
 # the Free Software Foundation, either version 2 of the License, or
 # (at your option) any later version.
 #
 # This program is distributed in the hope that it will be useful,
 # but WITHOUT ANY WARRANTY; without even the implied warranty of
 # MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 # GNU General Public License for more details.
 #
 # You should have received a copy of the GNU General Public License
 # along with this program.  If not, see <http://www.gnu.org/licenses/>.
 #
 */

#include "kv2csv.h"

#include "stropr.h"
#include "zeromalloc.h"

// Prototype functions
int insert2hash(struct hashtable *h, char *ptr, char sep, char *recordkey, char *valuesep);
int print2csv(struct hashtable *h, char sep, char *sortkey, int reccount, FILE *fout);
void usage(int verbose);
void usageverbose();
char wgetopt(int argc, char **argv, char *format);
int equalkeys(void *k1, void *k2);
unsigned int hashfromkey(void *ky);
int checkinfileformat(char *s);


char *version = "0.3.5";

void usage(verbose)
int verbose;
{
    printf("kv2csv -i filename [-o filename] -b key [-s key] [-f char] [-v] [-u]\n");
    printf("-i the filename to parse.\n");
    printf("-o file name of the csv file. If not set the file is written to stdout.\n");
    printf("-b the key name to use as record separator.\n");
    printf("-s the key name to use as the first column, default same as -b.\n");
    printf("-f the field separator between key and value, default is space\n");
    printf("-d the delimiter used between multiple occurrence of the same tag for a record, default is ,\n");
    printf("-v verbose processing and adding a : character and newline on the out put file.\n");
    printf("-u show usage. Use with -v to see extended usage\n\n");
    printf("kv2csv version %s\n\n", version);
    printf("kv2csv transforms a key/value file to a csv \n");
    printf("Return values\n"
           "1 - File names are not set\n"
           "2 - File do not exist\n"
           "3 - The separator character is not a single character\n"
           "4 - Must set key separator name\n"
           "5 - Can not create hashtable\n"
           "6 - Can not allocate enough memory\n"
           "7 - Failed to insert key and value to hashtable\n");

    if (verbose == 1)
        usageverbose();
}

void usageverbose()
{
    printf("kv2csv will transform a file of key/value records to a csv file.\n"
           "0001 John\n"
           "0002 Wayne\n"
           "0003 Cowboy\n"
           "0001 Harrison\n"
           "0002 Ford\n"
           "0003 Space Hero\n"
           "0004 Cool\n"
           "This example is a file with two records. The record separator key is\n"
           "0001 and must exist in all records. It must also be the first key in the\n"
           "record for all records.\n"
           " \n"
           "$ kv2csv -i mytest -b 0001\n"
           "\"0001\";\"0002\";\"0003\";\"0004\";\n"
           "\"John\";\"Wayne\";\"Cowboy\";\"\";\n"
           "\"Harrison\";\"Ford\";\"Space Hero\";\"Cool\";\n"
           "So even if not all keys exists for all records that will just be a empty\n"
           "cell.\n"
           "You can also change the order of the keys \n"
           "$ kv2csv -i mytest -b 0001 -s 0003\n"
           "\"0003\";\"0001\";\"0002\";\"0004\";\n"
           "\"Cowboy\";\"John\";\"Wayne\";" ";\n"
           "\"Space Hero\";\"Harrison\";\"Ford\";\"Cool\";\n"
           "\n"
           "The key do not need to be number just some string.\n"
           "First John\n"
           "Last Wayne\n"
           "Job Cowboy\n"
           "First Harrison\n"
           "Last Ford\n"
           "Job Space Hero\n"
           "Rate Cool\n"
           "\n"
           "$ kv2csv -i mytest -b First -s Last\n"
           "\"Last\";\"First\";\"Job\";\"Rate\";\n"
           "\"Wayne\";\"John\";\"Cowboy\";\"\";\n"
           "\"Ford\";\"Harrison\";\"Space Hero\";\"Cool\";\n"
           "\n"
           "First John\n"
           "Last Wayne\n"
           "Job Cowboy\n"
           "Job Actor\n"
           "First Harrison\n"
           "Last Ford\n"
           "Job Space Hero\n"
           "Rate Cool\n"
           "Rate Hard\n"
           "\n"
           "Its also possible to have multiple occurrence of a key, except the for \n"
           "record key delimiter.\n"
           "\n"
           "First John\n"
           "Last Wayne\n"
           "Job Cowboy\n"
           "Job Actor\n"
           "First Harrison\n"
           "Last Ford\n"
           "Job Space Hero\n"
           "Rate Cool\n"
           "Rate Hard\n"
           "\n"
           "\"First\";\"Job\";\"Last\";\"Rate\";\n"
           "\"John\";\"Cowboy,Actor\";\"Wayne\";\"\";\n"
           "\"Harrison\";\"Space_Hero\";\"Ford\";\"Cool,Hard\"\n"
           "\n"
           "For multiple occurrence there will be a list of comma separated values.\n");

}
DEFINE_HASHTABLE_INSERT(insert_some, struct key, struct value);
DEFINE_HASHTABLE_SEARCH(search_some, struct key, struct value);
DEFINE_HASHTABLE_REMOVE(remove_some, struct key, struct value);
DEFINE_HASHTABLE_ITERATOR_SEARCH(search_itr_some, struct key);


int verbose = 0;

int main(argc, argv)
int argc;
char **argv;
{

    FILE *fid = NULL;
    FILE *fout = NULL;

    int option;
    extern char *optarg;

    struct stat sb;
    char *infileptr = NULL;
    char *recordkey = NULL;
    char *sortkey = NULL;

    struct hashtable *h;
    int retstat;
    int reccount = 0;
    char sep = ' ';
    //int verbose = 0;
    int ret = 0;
    int showusage = 0;
    char valuesep[2];
    valuesep[0] = ',';
    valuesep[1] = '\0';

    while ((option = wgetopt(argc, argv, "i:o:b:s:f:d:vu" )) != -1)
        switch (option) {
        // -i the name of in file
        case 'i':
            fid = fopen(optarg, "rb");
            if (fid == NULL) {
                printf("In file do not exist - exit\n");
                exit(2);
            }
            stat(optarg, &sb);
            infileptr = (char *)zeromalloc(sb.st_size + 1);
            if (infileptr == NULL) {
                printf("Can not allocate enough memory - exiting\n");
                exit(6);
            }
            retstat = fread(infileptr, 1, sb.st_size, fid);
            *(infileptr + sb.st_size) = '\0';
            fclose(fid);
            break;
        // -o the name of the out fixed record file
        case 'o':
            fout = fopen(optarg, "w+");
            break;
        // -s the separator character, default ;
        case 'f':
            if (strlen(optarg) > 1) {
                printf("Separator is bigger then a single character - exiting\n");
                exit(3);
            }
            sep = *optarg;
            break;
        case 'd':
            if (strlen(optarg) > 1) {
                printf("Separator is bigger then a single character - exiting\n");
                exit(3);
            }
            valuesep[0] = *optarg;
            break;
        case 'b':
            recordkey = (char *)zeromalloc(strlen(optarg) + 1);
            recordkey = strcpy(recordkey, optarg);
            break;
        case 's':
            sortkey = (char *)zeromalloc(strlen(optarg) + 1);
            strcpy(sortkey, optarg);
            break;
        case 'v':
            verbose = 1;
            break;
        // format file
        case 'u':
            showusage = 1;
            break;
        default:
            break;
        }

    if (showusage == 1) {
        usage(verbose);
        exit(0);
    }

    // Check all conditions
    if (fid == NULL ) {
        printf("In file name is not set.\n");
        exit(1);
    }else if (fout == NULL )
        fout = fdopen(1, "w");

    if (recordkey == NULL) {
        printf("-b must be set\n");
        exit(4);
    }

    if (sortkey == NULL) {
        sortkey = (char *)zeromalloc(strlen(recordkey) + 1);
        strcpy(sortkey, recordkey);
    }

    

    // Create a hash table
    h = create_hashtable(16, hashfromkey, equalkeys);

    if (NULL == h) {
        printf("Can not create hashtable, exiting\n");
        exit(5);
    }

    infileptr = removeCR(infileptr);
    ret = checkinfileformat(infileptr);
    if (ret != 0) 
        exit(ret);
    infileptr = formatkv(infileptr, sep);
    reccount = insert2hash(h, infileptr, sep, recordkey, valuesep);
    print2csv(h, ';', sortkey, reccount, fout);
    fclose(fout);
    exit(0);
}

int insert2hash(h, ptr, sep, recordkey, valuesep)
struct hashtable *h;
char *ptr;
char sep;
char *recordkey;
char *valuesep;
{
    struct key *k;
    struct value *v, *found;

    char *parse;
    char *keystart;
    char *keyend;
    char *valuestart;
    char *valueend;
    int count = 0;
    parse = ptr;
    char *tmp;
    int initcount;

    while (*parse != '\0') {

        // Check for any special separator character and make correct alignment
        keystart = parse;
        keyend = parse;

        while (*keyend != sep)
            keyend++;

        *keyend = '\0';

        k = (struct key *)malloc(sizeof(struct key));
        if (NULL == k) {
            printf("ran out of memory allocating a key\n");
            return 1;
        }
        k->keyname = (char *)zeromalloc(strlen(keystart) + 1 );
        strcat(k->keyname, keystart);

        if (strcmp(k->keyname, recordkey) == 0 )
            count++;

        keyend++;
        valuestart = keyend;
        valueend = valuestart;
        while (*valueend != '\n')
            valueend++;
        *valueend = '\0';


        if (NULL == (found = search_some(h, k))) {

            v = (struct value *)malloc(sizeof(struct value));
            // Init all values
            for (initcount = 0; initcount < MAXRECORDS; initcount++)
                v->thevalue[initcount] = NULL;

            v->thevalue[count] = (char *)zeromalloc(strlen(valuestart) + 1 );
            strcat(v->thevalue[count], valuestart);

            //printf ("INSERT %d - value %s\n", count, valuestart);

            if (!insert_some(h, k, v)) { 
                printf("Can not insert to hashtable\n");
                exit(7);  /*oom*/
            }
        }else {
            if (found->thevalue[count] == NULL) {
                found->thevalue[count] = (char *)zeromalloc(strlen(valuestart) + 1 );
                strcat(found->thevalue[count], valuestart);
                //printf ("UPDATE %d - value %s\n", count, valuestart);
            }else {
                tmp = (char *)zeromalloc(strlen(found->thevalue[count]) + 1);
                strcat(tmp, found->thevalue[count]);
                found->thevalue[count] = (char *)zeromalloc(strlen(found->thevalue[count]) + strlen(valuestart) + 2 );
                strcat(found->thevalue[count], tmp);
                strcat(found->thevalue[count], valuesep);
                strcat(found->thevalue[count], valuestart);
                //printf ("UPDATE SAME %d - value %s\n", count, valuestart);
            }

        }
        valueend++;
        parse = valueend;

    }
    if (verbose)
        printf("After insertion, hashtable contains %u items.\n", hashtable_count(h));
    if (verbose)
        printf("Total records processed is %d\n", count);
    return count;
}

int print2csv(h, sep, sortkey, reccount, fout)
struct hashtable *h;
char sep;
char *sortkey;
int reccount;
FILE *fout;
{
    struct key *k, *sk;
    struct value *v, *sv;
    struct hashtable_itr *itr;
    int j;

    itr = hashtable_iterator(h);

    sk = (struct key *)malloc(sizeof(struct key));
    if (NULL == sk) {
        printf("ran out of memory allocating a key\n");
        return 1;
    }

    sk->keyname = (char *)zeromalloc(strlen(sortkey) + 1 );
    strcat(sk->keyname, sortkey);

    /*
     * Print the header row from keyname
     */

    if (hashtable_count(h) > 0) {
        fprintf(fout, "\"%s\"%c", sk->keyname, sep);
        do {
            k = hashtable_iterator_key(itr);
            if (strcmp(k->keyname, sortkey) != 0)
                fprintf(fout, "\"%s\"%c", k->keyname, sep);
        } while (hashtable_iterator_advance(itr));
        fprintf(fout, "\n");
    }

    free(itr);

    /*
     * Print the values
     */
    itr = hashtable_iterator(h);
    if (hashtable_count(h) > 0) {
        sv = search_some(h, sk);

        for (j = 1; j < reccount + 1; j++) {

            fprintf(fout, "\"%s\"%c", sv->thevalue[j], sep);

            itr = hashtable_iterator(h);
            do {

                k = hashtable_iterator_key(itr);
                if (strcmp(k->keyname, sortkey) != 0) {
                    v = hashtable_iterator_value(itr);
                    if (v->thevalue[j] == NULL)
                        fprintf(fout, "\"\"%c", sep);
                    else
                        fprintf(fout, "\"%s\"%c", v->thevalue[j], sep);
                }
            } while (hashtable_iterator_advance(itr));
            fprintf(fout, "\n");
        }
    }
    return 0;
}

int checkinfileformat(s)
char *s;
{
    int length = strlen(s);
    if (*(s+length-1) != '\n') {
        printf("In file do not end with a newline\n");
        return 1;
    }
    return 0;
}
