/*
 #
 # Copyright (C) 2009 Ingenjorsbyn AB
 #
 # This program is free software: you can redistribute it and/or modify
 # it under the terms of the GNU General Public License as published by
 # the Free Software Foundation, either version 2 of the License, or
 # (at your option) any later version.
 #
 # This program is distributed in the hope that it will be useful,
 # but WITHOUT ANY WARRANTY; without even the implied warranty of
 # MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 # GNU General Public License for more details.
 #
 # You should have received a copy of the GNU General Public License
 # along with this program.  If not, see <http://www.gnu.org/licenses/>.
 #
 */

/*
 * zeromalloc.c
 *
 *  Created on: Sep 12, 2009
 *      Author: andersh
 */
#include <stdio.h>
#include <stdlib.h>

void *zeromalloc(size)
unsigned int size;
{
    void *m;
    m = NULL;
    m = calloc(1, (size_t)size);
    //m = malloc((size_t)size);
    return m;
}


void zero(str, size)
char *str;
unsigned int size;
{
    char *mptr;
    int i = 0;

    mptr = str;
    // NULL all data
    for (i = 0; i < size; i++) {
        *mptr = '\0';
        mptr++;
    }
}
