/*
 #
 # Copyright (C) 2009 Ingenjorsbyn AB
 #
 # This program is free software: you can redistribute it and/or modify
 # it under the terms of the GNU General Public License as published by
 # the Free Software Foundation, either version 2 of the License, or
 # (at your option) any later version.
 #
 # This program is distributed in the hope that it will be useful,
 # but WITHOUT ANY WARRANTY; without even the implied warranty of
 # MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 # GNU General Public License for more details.
 #
 # You should have received a copy of the GNU General Public License
 # along with this program.  If not, see <http://www.gnu.org/licenses/>.
 #
 */

#include "kv2csv.h"

int
equalkeys(void *k1, void *k2)
{
    int comp;

    comp = strcmp(((struct key *)k1)->keyname, ((struct key *)k2)->keyname);

    if ( comp == 0)
        return 1;
    else
        return 0;
    //return (0 == memcmp(k1,k2,sizeof(struct key)));
}


unsigned int
hashfromkey(void *ky)
{
    struct key *k = (struct key *)ky;

    int h = 0;
    int n = 0;
    char *tmp;

    n = strlen(k->keyname);
    tmp = k->keyname;
    int i = 0;
    for (i = 0; i < n; i++) {
        h = 31 * h + *tmp;
        tmp++;
    }

    return h;
}

